package com.enverus.tnr.edm.marketview.client;

import com.datagenicgroup.curvebuilder.entity.ContractData;
import com.datagenicgroup.curvebuilder.entity.Product;
import com.datagenicgroup.curvebuilder.entity.ProductFolder;
import com.datagenicgroup.timeseries.ObjectObservation;
import com.enverus.tnr.edm.marketview.entity.MarketViewRequest;
import com.enverus.tnr.edm.marketview.entity.MarketViewResponse;

import java.util.List;

public interface MarketViewClient {

    public MarketViewResponse<List<Product>> getProducts(MarketViewRequest request);

    public MarketViewResponse<List<Product>> getProductsForFolder(MarketViewRequest request);

    public MarketViewResponse<List<ProductFolder>> getProductFolders(MarketViewRequest request);

    public MarketViewResponse<List<ContractData>> getContractData(MarketViewRequest request);

    public MarketViewResponse<List<String>> getContractPeriods(MarketViewRequest request);

    public MarketViewResponse<List<ObjectObservation>> getTimeSeriesObservations(MarketViewRequest request);
}
