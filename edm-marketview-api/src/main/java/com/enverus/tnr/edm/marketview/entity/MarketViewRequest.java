package com.enverus.tnr.edm.marketview.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;

@Getter
@Setter
@Accessors(chain = true)
public class MarketViewRequest {
    private MarketViewCredentials credentials;
    @Nonnull
    private MarketViewSearchCriteria marketViewSearchCriteria;
}
