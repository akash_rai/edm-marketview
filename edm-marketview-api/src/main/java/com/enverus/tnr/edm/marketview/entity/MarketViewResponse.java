package com.enverus.tnr.edm.marketview.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class MarketViewResponse<T> {
    Map<String, String> errorMap = new HashMap<>(2);
    private T response;
}
