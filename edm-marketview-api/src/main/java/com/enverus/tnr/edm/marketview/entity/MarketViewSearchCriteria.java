package com.enverus.tnr.edm.marketview.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class MarketViewSearchCriteria {
    private int barInterval = Integer.MIN_VALUE;
    private String category;
    private String commodity;
    private int daysBack = Integer.MIN_VALUE;
    private LocalDateTime endDate;
    private String exchange;
    private String folderId;
    private String marketBasis;
    private LocalDate onDate;
    private int page = -1;
    private int pageSize;
    private String product;
    private String productCode;
    private String profile;
    private List<String> profiles = Collections.emptyList();
    private String search;
    private boolean settledOnly;
    private LocalDateTime startDate;
    private String supplier;
    private String userId;
}
