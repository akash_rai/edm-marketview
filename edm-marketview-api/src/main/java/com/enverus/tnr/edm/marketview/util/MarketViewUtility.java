package com.enverus.tnr.edm.marketview.util;

import com.enverus.tnr.edm.marketview.entity.MarketViewCredentials;

import java.util.Map;

public class MarketViewUtility {
    private MarketViewUtility(){
        //utility
    }

    public static void logErrorToMap(Map<String, String> errorMap, ErrorCode errorCode, String... messages) {
        if (errorMap != null) {
            StringBuilder completeMessage = new StringBuilder(errorCode.getMessage());
            for (String message : messages) {
                completeMessage.append(" ");
                completeMessage.append(message);
            }
            errorMap.put(errorCode.getCode(), completeMessage.toString());
        }
    }

}
