package com.enverus.tnr.edm.marketview.util;

import lombok.Getter;

public enum ErrorCode {
    CUSTOM("MV-00", ""),
    MV_EXCEPTION("MV-03", "MarketView Exception"), //TODO add mv exceptions?
    EXCEPTION("MV-04", "An exception occurred"),
    NO_DATA("MV-02", "No Data "),
    REQUIRED_FIELD("MV-01", "Required Field");

    @Getter
    private final String code;
    @Getter
    private final String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return "[" + code + "]:" + message;
    }
}