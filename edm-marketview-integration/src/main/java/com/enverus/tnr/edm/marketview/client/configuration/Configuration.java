package com.enverus.tnr.edm.marketview.client.configuration;

import com.google.common.base.Strings;

public class Configuration {
    private String avalanche = "https://query.gvsi.com";
    private String webservice = "http://webservice.gvsi.com/gvsi/xml";
    private String server = "mvmts6.gvsi.com";
    private int port = 443;
    private boolean encrypted = true;
    private String userAgent;
    private String userAgentParams;
    private String username;
    private String password;
    private boolean live;
    private int periodCodeCacheExpiry = 60;
    private String mongoDB;
    private String databaseName;

    public String getAvalanche() {
        return avalanche;
    }

    public Configuration setAvalanche(String avalanche) {
        this.avalanche = avalanche;
        return this;
    }

    public String getWebservice() {
        return webservice;
    }

    public Configuration setWebservice(String webservice) {
        this.webservice = webservice;
        return this;
    }

    public String getServer() {
        return server;
    }

    public Configuration setServer(String server) {
        this.server = server;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Configuration setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Configuration setPassword(String password) {
        this.password = password;
        return this;
    }

    public boolean isLive() {
        return live;
    }

    public Configuration setLive(boolean live) {
        this.live = live;
        return this;
    }

    public int getPeriodCodeCacheExpiry() {
        return periodCodeCacheExpiry;
    }

    public Configuration setPeriodCodeCacheExpiry(int periodCodeCacheExpiry) {
        this.periodCodeCacheExpiry = periodCodeCacheExpiry;
        return this;
    }

    public int getPort() {
        return port;
    }

    public Configuration setPort(int port) {
        this.port = port;
        return this;
    }

    public boolean isEncrypted() {
        return encrypted;
    }

    public Configuration setEncrypted(boolean encrypted) {
        this.encrypted = encrypted;
        return this;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public Configuration setUserAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public String getUserAgentParams() {
        return userAgentParams;
    }

    public Configuration setUserAgentParams(String userAgentParams) {
        this.userAgentParams = userAgentParams;
        return this;
    }

    public String getMongoDB() {
        if (mongoDB == null)
            mongoDB = System.getenv("MONGODB_URL");
        return mongoDB;
    }

    public Configuration setMongoDB(String mongoDB) {
        this.mongoDB = mongoDB;
        return this;
    }

    public String getDatabaseName() {
        if (Strings.isNullOrEmpty(databaseName)){
            return "marketview";
        }
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }
}
