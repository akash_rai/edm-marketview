package com.enverus.tnr.edm.marketview.client.parser;

import com.datagenicgroup.curvebuilder.entity.ContractData;
import com.datagenicgroup.curvebuilder.entity.PeriodCodeValue;
import com.google.common.base.Strings;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Setter
@Accessors(chain = true)
public class ContractDataHandler extends AvalancheXMLHandler {

    private static final Logger logger = LoggerFactory.getLogger(ContractDataHandler.class);
    private final PeriodCodeTransformer periodCodeTransformer;
    @Getter
    private List<String> symbols = new ArrayList<>();
    @Getter
    private ContractData contractData = null;
    private String profile = null;
    private String root = null;

    private String periodCode = null;

    @Override
    public void execute(String csvResults) {
        String[] parts = csvResults.split("\n");
        int rowSize = parts.length;
        for (int i = 0; i < rowSize - 1; i++) {
            String line = parts[i];
            String[] lineParts = line.split(",");
            String symbol = lineParts[0].replace("\"", "");
            String symbolValue = lineParts[1].replace("\"", "");
            double value = Double.NaN;
            try {
                value = Double.parseDouble(symbolValue);
            } catch (Exception ignored) {
                //no op
            }

            if (!Strings.isNullOrEmpty(symbol) && !Double.isNaN(value)) {
                if (contractData == null)
                    contractData = new ContractData(null, null, null, null, -1);
                symbols.add(symbol);
                if (profile != null) {
                    String code;
                    if (symbol.equalsIgnoreCase(root) && periodCode != null)
                        code = periodCode;
                    else
                        code = periodCodeTransformer.convertPeriodCode(symbol, root);
                    if (code != null) {
                        contractData.addValue(new PeriodCodeValue().setValue(value).setPeriodCode(code));
                    }
                }
            }
        }
    }

    public void executeForwardContractData(String csvResults, String profile) {
        String[] parts = csvResults.split("\n");
        int rowSize = parts.length;

        for (int i = 0; i < rowSize - 1; i++) {
            String line = parts[i];
            String[] lineParts = line.split(",");
            if (lineParts.length > 0) {
                String symbol = lineParts[0].replace("\"", "");
                String symbolValue = getForwardContractData(lineParts, profile).replace("\"", "");
                double value = Double.NaN;
                try {
                    value = Double.parseDouble(symbolValue);
                } catch (Exception ignored) {
                    //no op
                }
                if (!Strings.isNullOrEmpty(symbol) && !Double.isNaN(value)) {
                    if (contractData == null)
                        contractData = new ContractData(null, null, null, null, -1);
                    symbols.add(symbol);

                    if (profile != null) {
                        String code;
                        if (symbol.equalsIgnoreCase(root) && periodCode != null)
                            code = periodCode;
                        else
                            code = periodCodeTransformer.convertPeriodCode(symbol, root);
                        if (code != null) {
                            contractData.addValue(new PeriodCodeValue().setValue(value).setPeriodCode(code));
                            logger.debug("Adding ForwardContract data for symbol:{},period:{},value:{}", symbol, code, value);
                        }
                    }
                }
            }
        }
    }

    public void executeDailyData(String csvResults, String profile) {
        String[] parts = csvResults.split("\n");
        int rowSize = parts.length;
        for (int i = 0; i < rowSize - 1; i++) {
            String line = parts[i];
            String[] lineParts = line.split(",");
            if (lineParts.length > 0) {
                String symbol = lineParts[0].replace("\"", "");
                String pval = getDailyData(lineParts, profile);
                String svalue = pval.replace("\"", "");
                double value = Double.NaN;
                try {
                    value = Double.parseDouble(svalue);
                } catch (Exception ignored) {
                    //no op
                }
                if (!Strings.isNullOrEmpty(symbol) && !Double.isNaN(value)) {
                    if (contractData == null)
                        contractData = new ContractData(null, null, null, null, -1);
                    symbols.add(symbol);
                    if (profile != null) {
                        String code;
                        if (symbol.equalsIgnoreCase(root) && periodCode != null)
                            code = periodCode;
                        else
                            code = periodCodeTransformer.convertPeriodCode(symbol, root);
                        if (code != null) {
                            contractData.addValue(new PeriodCodeValue().setValue(value).setPeriodCode(code));
                            logger.debug("Adding daily data for symbol:{},period:{},value:{}", symbol, code, value);
                        }
                    }
                }
            }
        }
    }

    private String getForwardContractData(String[] lineParts, String profile) {
        try {
            if (lineParts != null) {
                if (lineParts.length == 2)
                    return lineParts[1];
                if (lineParts.length > 1 && "OPEN".equalsIgnoreCase(profile))
                    return lineParts[1];
                if (lineParts.length > 2 && "HIGH".equalsIgnoreCase(profile))
                    return lineParts[2];
                if (lineParts.length > 3 && "LOW".equalsIgnoreCase(profile))
                    return lineParts[3];
                if (lineParts.length > 4 && "CLOSE".equalsIgnoreCase(profile))
                    return lineParts[4];
                if (lineParts.length > 5 && "VOLUME".equalsIgnoreCase(profile))
                    return lineParts[5];
                if (lineParts.length > 6 && "MIDPOINT".equalsIgnoreCase(profile))
                    return lineParts[6];
            }
        } catch (Exception ignored) {
            //no op
        }
        return "";
    }


    private String getDailyData(String[] lineParts, String profile) {
        try {
            if (lineParts != null) {
                if (lineParts.length == 2)
                    return lineParts[1];
                if (lineParts.length > 2 && "OPEN".equalsIgnoreCase(profile))
                    return lineParts[2];
                if (lineParts.length > 3 && "HIGH".equalsIgnoreCase(profile))
                    return lineParts[3];
                if (lineParts.length > 4 && "LOW".equalsIgnoreCase(profile))
                    return lineParts[4];
                if (lineParts.length > 5 && "CLOSE".equalsIgnoreCase(profile))
                    return lineParts[5];
                if (lineParts.length > 6 && "VOLUME".equalsIgnoreCase(profile))
                    return lineParts[6];
                if (lineParts.length > 7 && "MIDPOINT".equalsIgnoreCase(profile))
                    return lineParts[7];
            }
        } catch (Exception ignored) {
            //no op
        }
        return "";
    }
}
