package com.enverus.tnr.edm.marketview.client.rest;


import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.Base64;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class RESTQueryHelper {
    private static final Logger logger = LoggerFactory.getLogger(RESTQueryHelper.class);
    private static final String TIMER_NAME = "edm.marketview.request";

    private int timeout = 30000; // 30 seconds
    private int retries = 3;
    private MeterRegistry meterRegistry;

    public RESTQueryHelper() {
    }

    public RESTQueryHelper(MeterRegistry meterRegistry) {
        Objects.requireNonNull(meterRegistry, "meter registry is required");
        this.meterRegistry = meterRegistry;
        logger.trace("Creating request timer...");
        Timer requestTimer = meterRegistry.timer(TIMER_NAME);
        logger.trace("Created request timer");
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

    // Added new method using timeouts
    public String getRequest(String userName, String password, String url, Map<String, String> errorMap) {
        if(!errorMap.isEmpty())
            return "";
        String authString = userName + ":" + password;
        String authCode = Base64.getEncoder().encodeToString(authString.getBytes());
        String result = "";

        URI uri = URI.create(url);
        CloseableHttpResponse response = null;
        MDC.MDCCloseable mdcCloseableURLPath = MDC.putCloseable("url_path", uri.getPath());
        MDC.MDCCloseable mdcCloseableURLHost = MDC.putCloseable("url_host", uri.getHost());
        MDC.MDCCloseable mdcCloseableTransactionID = MDC.putCloseable("transaction_id", UUID.randomUUID().toString());

        Timer.Sample sample = null;
        if (!Objects.isNull(meterRegistry)) {
            sample = Timer.start(meterRegistry);
        }
        int status = HttpStatus.SC_BAD_REQUEST;
        try {
            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(timeout)
                    .setConnectionRequestTimeout(timeout)
                    .setSocketTimeout(timeout).build();
            HttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(retries, true);
            CloseableHttpClient httpClient = HttpClientBuilder
                    .create()
                    .setDefaultRequestConfig(config)
                    .setRetryHandler(retryHandler)
                    .build();
            HttpGet request = new HttpGet(url);
            request.addHeader("Authorization", "Basic " + authCode);
            request.addHeader("accept", "application/json");
            logger.debug("MV:REQUEST timeout[{}] retries[{}] {}", timeout, retries, url);
            response = httpClient.execute(request);
            if (!Objects.isNull(response.getEntity())) {
                result = EntityUtils.toString(response.getEntity());
            }
            status = response.getStatusLine().getStatusCode();
            logger.debug("MV:RESPONSE status[{}] size[{}] {}", status, result.length(), url);
            if ("status=\"OK\"".equalsIgnoreCase(result.trim())) {
                logger.warn("Empty payload received from MV for {}", url);
            }
            if (status != 200) {
                errorMap.put("MV-" + status, response.getStatusLine().getReasonPhrase());
            }
        } catch (SocketTimeoutException ste) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            logger.warn("MV:EXCEPTION status[{}] {}", status, url);
            errorMap.put("MV-" + status, "Request Timeout");
        } catch (Exception ex) {
            logger.warn("MV:EXCEPTION status[" + HttpStatus.SC_BAD_REQUEST + "]" + url, ex);
            errorMap.put("MV-"+ status, ex.getMessage());
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
            } catch (IOException ignored) {

            }
            mdcCloseableURLPath.close();
            mdcCloseableURLHost.close();
            mdcCloseableTransactionID.close();
            if (!Objects.isNull(meterRegistry)) {
                Timer timer = meterRegistry.timer(TIMER_NAME, "status", Integer.toString(status));
                sample.stop(timer);
            }
        }

        return result;
    }
}
