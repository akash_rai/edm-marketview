package com.enverus.tnr.edm.marketview.client.parser;

import com.enverus.tnr.edm.marketview.client.data.MarketViewQuote;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;

public class ChainHandler extends AvalancheXMLHandler {
    private boolean collectQuotes = false;
    private final List<String> symbols = new ArrayList<>();
    private final List<MarketViewQuote> quotes = new ArrayList<>();
    public List<String> getSymbols() {
        return symbols;
    }
    private String root;

    public List<MarketViewQuote> getQuotes() {
        return quotes;
    }

    public boolean isCollectQuotes() {
        return collectQuotes;
    }

    public ChainHandler setCollectQuotes(boolean collectQuotes) {
        this.collectQuotes = collectQuotes;
        return this;
    }

    public ChainHandler setRoot(String root) {
        this.root = root;
        return this;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if ("Item".equals(qName)) {
            String symbol = getString(attributes, "symbol");
            symbols.add(symbol);
            if (isCollectQuotes()) {
                MarketViewQuote quote = new MarketViewQuote().setSymbol(symbol).setKey(root);
                quote.setOpen(getDouble(attributes, "open"));
                quote.setHigh(getDouble(attributes, "high"));
                quote.setLow(getDouble(attributes, "low"));
                quote.setClose(getDouble(attributes, "close"));
                quote.setVolume(getDouble(attributes, "volume"));
                quotes.add(quote);
            }
        }
    }

}
