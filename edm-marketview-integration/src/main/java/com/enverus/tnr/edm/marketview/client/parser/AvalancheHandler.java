package com.enverus.tnr.edm.marketview.client.parser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.*;

/**
 * Created by colin.hartley on 07/06/2018.
 */
public class AvalancheHandler extends AvalancheXMLHandler {
    private Properties currentModel = null;
    private List<Properties> models = new ArrayList<>();
    private String field = null;
    private String text = null;
    private int count;

    public List<Properties> getModels() {
        return models;
    }

    public int getCount() {
        if (count < models.size())
            count = models.size();
        return count;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        switch (qName) {
            case "Row":
                currentModel = new Properties();
                break;
            case "Field":
                field = getString(attributes, "Name");
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        text = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        switch (qName) {
            case "Row":
                if (currentModel.size() > 0)
                    models.add(currentModel);
                currentModel = null;
                break;
            case "Field":
                if (field != null && text != null) {
                    currentModel.setProperty(field.toLowerCase(), text);
                }
        }
    }

    @Override
    public void execute(String csvResults) {
        properties = new HashMap<>();
        String[] parts = csvResults.split("\n");
        int max = parts.length;
        if (parts.length > 0) {
            String[] headerParts = parts[0].split(",");
            for (int i = 1; i < max; i++) {
                String[] lineParts = splitLine(parts[i]);
                int columnSize = lineParts.length;
                for (int j = 0; j < columnSize; j++) {
                    properties.put(headerParts[j].toLowerCase(), StripQuotes(lineParts[j]));
                }
            }
        }
    }

    private String StripQuotes(String str) {
        if (str.startsWith("\"") && str.endsWith("\"")) {
            str = str.substring(1, str.length() - 1);
        }
        return str;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
    public String[] splitLine(String line) {
        List<String> parts = new ArrayList<>();
        int ch = 0;
        while (ch < line.length()) {
            if (line.charAt(ch) == '"') {
                int cq = line.indexOf("\"", ch+1);
                parts.add(line.substring(ch+1, cq));
                ch = cq+1;
            } else {
                int cq = line.indexOf(",", ch);
                if (cq == -1) {
                    parts.add("");
                    break;
                } else {
                    parts.add(line.substring(ch, cq));
                    ch = cq;
                }
            }
            ch++;
        }
        return parts.toArray(new String[0]);
    }
}
