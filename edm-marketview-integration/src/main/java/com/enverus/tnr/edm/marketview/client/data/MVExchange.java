package com.enverus.tnr.edm.marketview.client.data;

public class MVExchange {

    private String category;
    private String code;
    private String template;

    public String getCode() {
        return code;
    }

    public MVExchange setCode(String code) {
        this.code = code;
        return this;
    }

    public String getTemplate() {
        return template;
    }

    public MVExchange setTemplate(String template) {
        this.template = template;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
