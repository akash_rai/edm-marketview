package com.enverus.tnr.edm.marketview.client.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketViewQuote {
    private String key;
    private String symbol;
    private String description;
    private String expirationdate;
    private String ask;
    private String bid;
    private String last;
    private String lastvolume;
    private String askdatetimeutc;
    private String biddatetimeutc;
    private double open;
    private double high;
    private double low;
    private double close;
    private double volume;

    public String getKey() {
        return key;
    }

    public MarketViewQuote setKey(String key) {
        this.key = key;
        return this;
    }

    public String getSymbol() {
        return symbol;
    }

    public MarketViewQuote setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public MarketViewQuote setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getExpirationdate() {
        return expirationdate;
    }

    public MarketViewQuote setExpirationdate(String expirationdate) {
        this.expirationdate = expirationdate;
        return this;
    }

    public String getAsk() {
        return ask;
    }

    public MarketViewQuote setAsk(String ask) {
        this.ask = ask;
        return this;
    }

    public String getBid() {
        return bid;
    }

    public MarketViewQuote setBid(String bid) {
        this.bid = bid;
        return this;
    }

    public String getLast() {
        return last;
    }

    public MarketViewQuote setLast(String last) {
        this.last = last;
        return this;
    }

    public String getLastvolume() {
        return lastvolume;
    }

    public MarketViewQuote setLastvolume(String lastvolume) {
        this.lastvolume = lastvolume;
        return this;
    }

    public String getAskdatetimeutc() {
        return askdatetimeutc;
    }

    public MarketViewQuote setAskdatetimeutc(String askdatetimeutc) {
        this.askdatetimeutc = askdatetimeutc;
        return this;
    }

    public String getBiddatetimeutc() {
        return biddatetimeutc;
    }

    public MarketViewQuote setBiddatetimeutc(String biddatetimeutc) {
        this.biddatetimeutc = biddatetimeutc;
        return this;
    }

    public double getOpen() {
        return open;
    }

    public MarketViewQuote setOpen(double open) {
        this.open = open;
        return this;
    }

    public double getHigh() {
        return high;
    }

    public MarketViewQuote setHigh(double high) {
        this.high = high;
        return this;
    }

    public double getLow() {
        return low;
    }

    public MarketViewQuote setLow(double low) {
        this.low = low;
        return this;
    }

    public double getClose() {
        return close;
    }

    public MarketViewQuote setClose(double close) {
        this.close = close;
        return this;
    }

    public double getVolume() {
        return volume;
    }

    public MarketViewQuote setVolume(double volume) {
        this.volume = volume;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MarketViewQuote) {
            MarketViewQuote other = (MarketViewQuote)obj;
            if (same(getAsk(), other.getAsk())
                    && same(getBid(), other.getBid())
                    && same(getLast(), other.getLast())
                    && same(getLastvolume(), other.getLastvolume())
                    && same(getAskdatetimeutc(), other.getAskdatetimeutc())
                    && same(getBiddatetimeutc(), other.getBiddatetimeutc()))
                return true;
            return false;
        }
        return super.equals(obj);
    }
    private boolean same(String a, String b) {
        if (a == null && b == null)
            return true;
        else if (a == null)
            return false;
        else if (b == null)
            return false;
        return a.equals(b);
    }
}
