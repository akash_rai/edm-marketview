package com.enverus.tnr.edm.marketview.client.parser;

import com.enverus.tnr.edm.marketview.client.rest.RESTQueryHelper;
import com.enverus.tnr.edm.marketview.client.data.MVSymbolPeriod;
import com.enverus.tnr.edm.marketview.client.configuration.Configuration;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static com.enverus.tnr.edm.marketview.client.parser.AvalancheConsumer.encode;
import static com.mongodb.client.model.Filters.eq;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class PeriodCodeTransformer {
    public static final String WEEK = "_WEEK";
    private static final Map<String, String> months = new TreeMap<>();
    private static final Map<String, String> weeks = new TreeMap<>();
    private static final Map<String, String> strips = new TreeMap<>();
    private static final Logger logger = LoggerFactory.getLogger(PeriodCodeTransformer.class);
    private static LoadingCache<String, String> cache;

    static {
        months.put("F", "M01");
        months.put("G", "M02");
        months.put("H", "M03");
        months.put("J", "M04");
        months.put("K", "M05");
        months.put("M", "M06");
        months.put("N", "M07");
        months.put("Q", "M08");
        months.put("U", "M09");
        months.put("V", "M10");
        months.put("X", "M11");
        months.put("Z", "M12");
        strips.put("F-H", "Q01");
        strips.put("J-M", "Q02");
        strips.put("N-U", "Q03");
        strips.put("V-Z", "Q04");
        strips.put("J-U", "SS");
        strips.put("V-H", "SW");
        strips.put("F-Z", "Y");

        strips.put("QF", "Q01");
        strips.put("QJ", "Q02");
        strips.put("QN", "Q03");
        strips.put("QV", "Q04");

        // Seasons
        strips.put("SPH", "SP");
        strips.put("SJ", "SS");
        strips.put("SMN", "SS");
        strips.put("SV", "SW");
        strips.put("WNF", "SW");

        // Years
        strips.put("YF", "Y");
        strips.put("CYF", "Y");
        strips.put("BALPY", "BOY");

        // Half Months
        strips.put("1F", "HM01");
        strips.put("2F", "HM02");
        strips.put("1G", "HM03");
        strips.put("2G", "HM04");
        strips.put("1H", "HM05");
        strips.put("2H", "HM06");
        strips.put("1J", "HM07");
        strips.put("2J", "HM08");
        strips.put("1K", "HM09");
        strips.put("2K", "HM10");
        strips.put("1M", "HM11");
        strips.put("2M", "HM12");
        strips.put("1N", "HM13");
        strips.put("2N", "HM14");
        strips.put("1Q", "HM15");
        strips.put("2Q", "HM16");
        strips.put("1U", "HM17");
        strips.put("2U", "HM18");
        strips.put("1V", "HM19");
        strips.put("2V", "HM20");
        strips.put("1X", "HM21");
        strips.put("2X", "HM22");
        strips.put("1Z", "HM23");
        strips.put("2Z", "HM24");

        //Weekly
        weeks.put("F", "01");
        weeks.put("G", "02");
        weeks.put("H", "03");
        weeks.put("J", "04");
        weeks.put("K", "05");
        weeks.put("M", "06");
        weeks.put("N", "07");
        weeks.put("Q", "08");
        weeks.put("U", "08");
        weeks.put("V", "10");
        weeks.put("X", "11");
        weeks.put("Z", "12");

    }

    @Nonnull
    private final MongoClient mongo;
    @Nonnull
    private final RESTQueryHelper restQueryHelper;
    @Nonnull
    private Configuration configuration;

    public PeriodCodeTransformer(@Nonnull MongoClient mongo,
                                 @Nonnull Configuration configuration,
                                 @Nonnull RESTQueryHelper restQueryHelper) {
        this.mongo = mongo;
        this.configuration = configuration;
        this.restQueryHelper = restQueryHelper;
    }

    public void setConfiguration(Configuration config) {
        configuration = config;
        cache = CacheBuilder.newBuilder().expireAfterWrite(configuration.getPeriodCodeCacheExpiry(), TimeUnit.SECONDS)
                .build(new CacheLoader<>() {
                    @Override
                    public String load(String s) {
                        String[] parts = s.split(" ");
                        return getPeriodCode(parts[0], parts[1]);
                    }
                });
    }

    private String getPeriodCode(String symbol, String root) {
        if (root != null && root.equals(symbol)) {
            String meta = readPeriodCodeFromMetadata(symbol);
            if (meta != null && meta.length() > 0) {
                return meta;
            }
        }
        MongoDatabase datastore = mongo.getDatabase(configuration.getDatabaseName());
        final CodecRegistry registry = fromRegistries(
                MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoCollection<MVSymbolPeriod> coll = datastore.getCollection("MVSymbolPeriod", MVSymbolPeriod.class).withCodecRegistry(registry);
        MVSymbolPeriod sp = coll.find(eq("symbol", symbol)).first();
        if (sp != null)
            return sp.getPeriod();

        String mvSymbol = symbol;
        String fin = financialPeriodCode(symbol);
        if (fin != null) {
            return fin;
        }

        if (root != null && !root.equals(symbol)) {

            try {
                if (!root.endsWith(WEEK) && root.contains("_"))
                    symbol = symbol.substring(root.split("_")[0].length());
                else
                    symbol = symbol.substring(root.length());
            } catch (Exception ex) {
                logger.error("==============String Index out of Bounds {} ====> {}", symbol, root);

            }
        } else if (symbol.length() < 4)
            return "SPOT";

        // The rest now tries to interpret the period code from the symbol
        String period = null;
        String pc;
        String letter = "";
        String syear;
        int year = 0;
        try {
            if (root != null && !root.endsWith(WEEK)) {
                pc = symbol.substring(symbol.length() - 3);
                letter = pc.substring(0, 1);
                period = months.get(letter);
                syear = pc.substring(1);
                year = Integer.parseInt(syear) + 2000;
            }
        } catch (Exception ignored) {
            return "SPOT";
        }

        // Test for strips
        if (symbol.length() > 3) {
            String st = symbol.substring(0, symbol.length() - 2);
            if (strips.get(st) != null) {
                period = strips.get(st);
            } else if (symbol.startsWith("-", symbol.length() - 4)) {
                pc = symbol.substring(symbol.length() - 7, symbol.length() - 4);
                String l0 = pc.substring(0, 1);
                period = strips.get(l0 + "-" + letter);
            }
            if (root != null && root.endsWith("W")) {
                try {
                    period = "W" + st;
                } catch (Exception e) {
                    return "SPOT";
                }
            }
        }

        // Handle weekly

        if (root != null && root.endsWith(WEEK)) {
            try {
                symbol = mvSymbol.substring(mvSymbol.length() - 4);
                pc = symbol.substring(symbol.length() - 3);
                syear = pc.substring(1);
                year = Integer.parseInt(syear) + 2000;
                String week = symbol.substring(0, symbol.length() - 3);
                String mvMonth = symbol.substring(1, symbol.length() - 2);
                String month = weeks.get(mvMonth);
                int weekOfYear = findWeekOfYear(year, month, week);
                String weekNumber = String.valueOf(weekOfYear);
                if (weekNumber.length() == 1)
                    weekNumber = Weeks.ZERO.getWeeks() + weekNumber;
                period = "W" + weekNumber;
                year = findYear(year, month, week);
            } catch (Exception e) {
                return "SPOT";
            }
        }

        // Test for daily
        if (symbol.length() > 3 && root != null && !root.endsWith("W")) {
            String day = symbol.substring(0, 2);
            try {
                Integer.parseInt(day);
                if (period != null)
                    period = period.substring(1) + day;
            } catch (Exception e) {
                // Not a number
            }
        }

        // Test for Quarters from root symbol
        String xpc = symbol;
        if (root == null)
            xpc = symbol.substring(symbol.length() - 4, symbol.length() - 3);
        String ptest = strips.get(xpc + letter);
        if (ptest != null)
            period = ptest;

        if (period == null) {
            return "SPOT";
        }
        return year + period;
    }

    private String readPeriodCodeFromMetadata(String symbol) {
        String meta = null;
        String url = configuration.getAvalanche() + "/UnifiedMetadata/csv?exact=1&search=" + encode(symbol);
        try {
            AvalancheHandler handler = new AvalancheHandler();
            String results = restQueryHelper.getRequest(configuration.getUsername(), configuration.getPassword(), url, new HashMap<>());
            handler.execute(results);
            meta = handler.getProperties().get("periodcd");
        } catch (Exception e) {
            logger.info("An unexpected error has occurred while fetching period code from Avalanche. Cause {}", e.getMessage());
        }
        return meta;
    }

    private static String financialPeriodCode(String symbol) {
        if (symbol.endsWith("ON"))
            return "ON";
        if (symbol.endsWith("TN"))
            return "TN";
        if (symbol.endsWith("SN"))
            return "SN";
        if (symbol.endsWith("SW"))
            return "SW";
        if (symbol.endsWith("W") || symbol.endsWith("M") || symbol.endsWith("Y")) {
            String per = symbol.substring(symbol.length() - 3, symbol.length() - 1);
            try {
                Integer.parseInt(per);
                return symbol.substring(symbol.length() - 3);
            } catch (Exception e) {
                per = symbol.substring(symbol.length() - 2, symbol.length() - 1);
                try {
                    Integer.parseInt(per);
                    return symbol.substring(symbol.length() - 2);
                } catch (Exception ex) {
                    // Not a number
                }
            }
        }
        return null;
    }

    private static int findWeekOfYear(int year, String month, String week) {
        int daysOfWeek = 7;
        int days = Integer.parseInt(week) * daysOfWeek;
        DateTime dateFrom = new DateTime().withYear(year).withMonthOfYear(Integer.parseInt(month));
        int dayOfMonth = dateFrom.dayOfMonth().getMaximumValue();
        if (days > dayOfMonth) {
            days = dayOfMonth;
        }
        dateFrom = dateFrom.withDayOfMonth(days);
        return dateFrom.getWeekOfWeekyear();
    }

    private static int findYear(int year, String month, String week) {
        int daysOfWeek = 7;
        int days = Integer.parseInt(week) * daysOfWeek;
        DateTime dateFrom = new DateTime().withYear(year).withMonthOfYear(Integer.parseInt(month));
        int dayOfMonth = dateFrom.dayOfMonth().getMaximumValue();
        if (days > dayOfMonth && Integer.parseInt(month) == Months.TWELVE.getMonths())
            return year + Years.ONE.getYears();
        return year;
    }

    @PostConstruct
    @SuppressWarnings("unused")
    public void init() {
        if (Strings.isNullOrEmpty(configuration.getDatabaseName())) {
            throw new IllegalArgumentException("Required field DatabaseName");
        }
        if (Strings.isNullOrEmpty(configuration.getAvalanche())) {
            throw new IllegalArgumentException("Required field AvalancheURL");
        }
        if (Strings.isNullOrEmpty(configuration.getUsername())) {
            throw new IllegalArgumentException("Required field Username");
        }
        if (Strings.isNullOrEmpty(configuration.getPassword())) {
            throw new IllegalArgumentException("Required field Password");
        }

        if (Objects.isNull(cache)) {
            cache = CacheBuilder.newBuilder().expireAfterWrite(configuration.getPeriodCodeCacheExpiry(), TimeUnit.SECONDS)
                    .build(new CacheLoader<>() {
                        @Override
                        public String load(String s) {
                            String[] parts = s.split(" ");
                            return getPeriodCode(parts[0], parts[1]);
                        }
                    });
        }
    }

    public String convertPeriodCode(String symbol) {
        return convertPeriodCode(symbol, null);
    }

    public String convertPeriodCode(String symbol, String root) {
        if (symbol == null)
            return null;
        if (cache == null)
            return getPeriodCode(symbol, root);
        try {
            return cache.get(symbol + " " + root);
        } catch (ExecutionException e) {
            return "SPOT";
        }
    }
}
