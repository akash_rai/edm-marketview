package com.enverus.tnr.edm.marketview.client.parser;

import com.datagenicgroup.timeseries.ObjectObservation;
import lombok.Getter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

public class TimeSeriesHandler extends AvalancheXMLHandler {
    @Getter
    private final List<ObjectObservation> observations = new ArrayList<>();
    DateTimeFormatter intraDayDateFormat = DateTimeFormat.forPattern("M/dd/YYYY hh:mm:ss a");
    DateTimeFormatter dateFormat = DateTimeFormat.forPattern("M/dd/YYYY");

    @Override
    public void execute(String csvResults) {
        String[] parts = csvResults.split("\n");
        int rowSize = parts.length;
        for (int i = 0; i < rowSize - 1; i++) {
            String line = parts[i];
            String[] lineParts = line.split(",");
            String dateTime = lineParts[1].replace("\"", "");
            String value = lineParts[2].replace("\"", "");
            String date = dateTime.split(" ")[0];
            DateTime index = dateFormat.parseDateTime(date);
            Double v = Double.parseDouble(value);
            observations.add(new ObjectObservation().setIndex(index).setValue(v));
        }
    }

    public void executeIntraDay(String csvResults) {
        String[] parts = csvResults.split("\n");
        for (int i = 0; i < parts.length - 1; i++) {
            String line = parts[i];
            String[] lineParts = line.split(",");
            String dateTime = lineParts[1].replace("\"", "");
            String value = lineParts[2].replace("\"", "");
            DateTime index = intraDayDateFormat.parseDateTime(dateTime);
            Double v = Double.parseDouble(value);
            observations.add(new ObjectObservation().setIndex(index).setValue(v));
        }
    }
}
