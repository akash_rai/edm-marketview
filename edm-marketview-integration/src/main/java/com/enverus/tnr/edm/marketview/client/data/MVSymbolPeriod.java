package com.enverus.tnr.edm.marketview.client.data;

import org.bson.codecs.pojo.annotations.BsonId;

import java.io.Serializable;

public class MVSymbolPeriod implements Serializable {
    private static final long serialVersionUID = 1L;
    @BsonId
    private String symbol;
    private String period;

    public String getSymbol() {
        return symbol;
    }

    public MVSymbolPeriod setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public String getPeriod() {
        return period;
    }

    public MVSymbolPeriod setPeriod(String period) {
        this.period = period;
        return this;
    }
}
