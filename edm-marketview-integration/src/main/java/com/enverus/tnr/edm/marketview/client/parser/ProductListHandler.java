package com.enverus.tnr.edm.marketview.client.parser;

import com.datagenicgroup.curvebuilder.entity.Product;
import com.datagenicgroup.curvebuilder.entity.ProductBuilder;
import com.google.common.base.Strings;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
@Accessors(chain = true)
public class ProductListHandler extends AvalancheXMLHandler {
    @Getter
    private final List<Product> products = new ArrayList<>();
    private boolean keystore = false;

    @Override
    public void execute(String csvResults) {

        String[] parts = csvResults.split("\r\n");
        if (parts.length > 0) {
            if (parts[0].equals("records") || parts[0].startsWith("ErrorCode") && parts.length == 2) {
                return;
            }
            String[] headerParts = parts[0].split(",");
            parseProducts(parts, headerParts);
        }
    }

    private void parseProducts(String[] parts, String[] headerParts) {
        for (int line = 1; line < parts.length; line++) {
            // Check for keystore
            if (parts[line].isEmpty()) {
                keystore = true;
                headerParts = parts[line + 1].split(",");
                line = line + 2;
            }
            String[] lineParts = splitLine(parts[line]);
            int max = Math.min(headerParts.length, lineParts.length);
            if (max > 1 && !keystore) {
                ProductBuilder builder = new ProductBuilder()
                        .setSource("MARKETVIEW")
                        .setDefaultProfile("CLOSE");
                parseProduct(headerParts, lineParts, max, builder);
                products.add(builder.build());
            }
        }
    }

    private void parseProduct(String[] headerParts, String[] lineParts, int max, ProductBuilder builder) {
        String unit = null;
        for (int i = 0; i < max; i++) {
            String propertyName = headerParts[i];
            String propertyValue = lineParts[i].replace("\"", "");
            if (propertyName.equalsIgnoreCase("description")) {
                builder.setDescription(propertyValue);
                builder.setName(propertyValue);
            } else if (propertyName.equalsIgnoreCase("exchange_code")) {
                builder.setCategory(propertyValue);
                builder.setSecurityPolicy("model://marketview/" + propertyValue);
            } else if (propertyName.equalsIgnoreCase("symbol")) {
                builder.setCode(propertyValue);
                builder.setId(propertyValue);
            } else if (propertyName.equalsIgnoreCase("commodity")) {
                builder.setCommodity(propertyValue);
            } else if (propertyName.equalsIgnoreCase("product")) {
                builder.setProduct(propertyValue);
            } else if (propertyName.equalsIgnoreCase("supplier")) {
                builder.setSupplier(propertyValue);
            } else if (propertyName.equalsIgnoreCase("marketbasis")) {
                builder.setMarketBasis(propertyValue);
            } else if (propertyName.equalsIgnoreCase("currency")) {
                builder.setCurrency(propertyValue);
            } else if (propertyName.equalsIgnoreCase("lotunit") || propertyName.equalsIgnoreCase("lot_units")) {
                builder.setUnits(propertyValue);
                unit = Strings.emptyToNull(propertyValue);
            } else if (propertyName.equalsIgnoreCase("conversion_factor") && !Strings.isNullOrEmpty(propertyValue)) {
                if (!"MT".equalsIgnoreCase(unit)) {
                    builder.add(new Product.CurveProperty().setName("VendorUnit").setValue("MT"));
                    builder.add(new Product.CurveProperty().setName("VendorFactor").setValue(propertyValue));
                } else {
                    builder.add(new Product.CurveProperty().setName("VendorUnit").setValue("BBL"));
                    builder.add(new Product.CurveProperty().setName("VendorFactor").setValue("" + (1 / Double.parseDouble(propertyValue))));
                }
            } else if (!Strings.isNullOrEmpty(propertyValue)) {
                builder.add(new Product.CurveProperty().setName(propertyName.toLowerCase()).setValue(propertyValue));
            }
        }
    }

    @Override
    public String[] splitLine(String line) {
        List<String> parts = new ArrayList<>();
        int ch = 0;
        while (ch < line.length()) {
            if (line.charAt(ch) == '"') {
                int cq = line.indexOf("\"", ch + 1);
                parts.add(line.substring(ch + 1, cq));
                ch = cq + 1;
            } else {
                int cq = line.indexOf(",", ch);
                if (cq == -1) {
                    parts.add("");
                    break;
                } else {
                    parts.add(line.substring(ch, cq));
                    ch = cq;
                }
            }
            ch++;
        }
        return parts.toArray(new String[0]);
    }
}
