package com.enverus.tnr.edm.marketview.client.impl;

import com.enverus.tnr.edm.marketview.client.parser.PeriodCodeTransformer;
import com.enverus.tnr.edm.marketview.client.parser.ChainHandler;
import com.enverus.tnr.edm.marketview.client.rest.RESTQueryHelper;
import com.datagenicgroup.curvebuilder.entity.ContractData;
import com.datagenicgroup.curvebuilder.entity.Product;
import com.datagenicgroup.curvebuilder.entity.Product.CurveProperty;
import com.datagenicgroup.curvebuilder.entity.ProductFolder;
import com.datagenicgroup.timeseries.ObjectObservation;
import com.enverus.tnr.edm.marketview.client.MarketViewClient;
import com.enverus.tnr.edm.marketview.client.parser.*;
import com.enverus.tnr.edm.marketview.entity.MarketViewCredentials;
import com.enverus.tnr.edm.marketview.entity.MarketViewRequest;
import com.enverus.tnr.edm.marketview.entity.MarketViewResponse;
import com.enverus.tnr.edm.marketview.entity.MarketViewSearchCriteria;
import com.enverus.tnr.edm.marketview.util.ErrorCode;
import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.enverus.tnr.edm.marketview.util.ErrorCode.*;
import static com.enverus.tnr.edm.marketview.util.MarketViewUtility.logErrorToMap;

@Slf4j
@RequiredArgsConstructor
public class MarketViewClientAPI implements MarketViewClient {

    private static final List<String> FC_SECURITY_TYPE = Arrays.asList("F", "SF");
    @Nonnull
    private final String avalanche;
    @Nonnull
    private final String webservice;
    @Nonnull
    private final RESTQueryHelper queryHelper;
    @Nonnull
    private final PeriodCodeTransformer transformer;

    private static void validateMandatoryParamsForGetTimeSeriesObservations(MarketViewRequest request, Map<String, String> errorMap) {
        MarketViewSearchCriteria searchCriteria = request.getMarketViewSearchCriteria();
        if (Strings.isNullOrEmpty(searchCriteria.getSearch()))
            logErrorToMap(errorMap, REQUIRED_FIELD, "search for time series observations");
        if (Strings.isNullOrEmpty(searchCriteria.getProfile()))
            logErrorToMap(errorMap, REQUIRED_FIELD, "profile for time series observations");
        if (Objects.isNull(searchCriteria.getStartDate()))
            searchCriteria.setStartDate(LocalDateTime.of(2000, 1, 1, 0, 0));
        if (Objects.isNull(searchCriteria.getEndDate()))
            searchCriteria.setEndDate(LocalDateTime.now());
    }

    private static void validateMandatoryParamsForGetContractPeriods(MarketViewRequest request, Map<String, String> errorMap) {
        if (Strings.isNullOrEmpty(request.getMarketViewSearchCriteria().getSearch())) {
            logErrorToMap(errorMap, REQUIRED_FIELD, "search for contract periods");
        }
    }

    private static void validateMandatoryParamsForGetProductFolders(MarketViewRequest request, Map<String, String> errorMap) {
        MarketViewSearchCriteria searchCriteria = request.getMarketViewSearchCriteria();
        if (Strings.isNullOrEmpty(searchCriteria.getUserId())) {
            logErrorToMap(errorMap, REQUIRED_FIELD, "user id for product folders");
        }
        if (Strings.isNullOrEmpty(searchCriteria.getFolderId())) {
            searchCriteria.setFolderId("1");
        }
    }

    private static void validateMandatoryParamsForGetProducts(MarketViewRequest request, Map<String, String> errorMap) {
        MarketViewSearchCriteria searchCriteria = request.getMarketViewSearchCriteria();
        if (Strings.isNullOrEmpty(searchCriteria.getUserId()) &&
                Strings.isNullOrEmpty(searchCriteria.getSearch())) {
            logErrorToMap(errorMap, REQUIRED_FIELD, "either userId or search for products");
        }
    }

    private static void validateMandatoryParamsForGetContractData(MarketViewRequest request, Map<String, String> errorMap) {
        MarketViewSearchCriteria searchCriteria = request.getMarketViewSearchCriteria();
        if (searchCriteria.getProfiles().isEmpty())
            logErrorToMap(errorMap, REQUIRED_FIELD, "profile for contract data");
        if (Strings.isNullOrEmpty(searchCriteria.getSearch()))
            logErrorToMap(errorMap, REQUIRED_FIELD, "search for contract data");
        if (!Objects.isNull(searchCriteria.getOnDate()) && Objects.isNull(searchCriteria.getStartDate()) && Objects.isNull(searchCriteria.getEndDate())) {
            searchCriteria.setStartDate(searchCriteria.getOnDate().atStartOfDay());
            searchCriteria.setEndDate(searchCriteria.getOnDate().atStartOfDay());
        }
    }

    private static void validateCredentials(MarketViewRequest request, Map<String, String> errorMap) {
        Objects.requireNonNull(request.getCredentials(),"MarketView Credentials can't be null");
        if (!request.getCredentials().isValid()) {
            logErrorToMap(errorMap, REQUIRED_FIELD, "username and password");
        }
    }

    private static boolean isForwardCurve(Product product) {
        if (product != null) {
            CurveProperty curveProperty = CurveProperty.getPropertyByName(product.getProperties(), "SECURITY_TYPE");
            return curveProperty != null && FC_SECURITY_TYPE.contains(Strings.emptyToNull(curveProperty.getValue()));
        }
        return false;
    }

    @Override
    public MarketViewResponse<List<Product>> getProducts(@Nonnull MarketViewRequest request) {
        final MarketViewResponse<List<Product>> response = new MarketViewResponse<>();
        final List<Product> products = new ArrayList<>();
        response.setResponse(products);

        validateCredentials(request, response.getErrorMap());
        validateMandatoryParamsForGetProducts(request, response.getErrorMap());

        Optional<String> optionalRawResponse = prepareURLForGetProducts(request, response.getErrorMap())
                .filter(var -> response.getErrorMap().isEmpty())
                .map(url -> queryHelper.getRequest(request.getCredentials().getUsername(),
                        request.getCredentials().getPassword(),
                        url, response.getErrorMap()));

        optionalRawResponse.filter(var -> response.getErrorMap().isEmpty()).ifPresent(rawResponse -> {
            ProductListHandler handler = new ProductListHandler();
            handler.execute(rawResponse);
            products.addAll(handler.getProducts());
        });
        if (products.isEmpty()) logErrorToMap(response.getErrorMap(), NO_DATA, "for product");
        return response;
    }

    @Override
    public MarketViewResponse<List<Product>> getProductsForFolder(@Nonnull MarketViewRequest request) {
        final MarketViewResponse<List<Product>> response = new MarketViewResponse<>();
        final List<Product> products = new ArrayList<>();
        response.setResponse(products);

        validateCredentials(request, response.getErrorMap());
        validateMandatoryParamsForGetProductFolders(request, response.getErrorMap());

        Optional<String> optionalRawResponse = prepareURLForGetProductFolders(request, response.getErrorMap())
                .filter(var -> response.getErrorMap().isEmpty())
                .map(url -> queryHelper.getRequest(request.getCredentials().getUsername() + "/" + request.getMarketViewSearchCriteria().getUserId(),
                        request.getCredentials().getPassword(),
                        url, response.getErrorMap()));

        optionalRawResponse.filter(var -> response.getErrorMap().isEmpty()).ifPresent(rawResponse -> {
            ProductFolderSymbolHandler handler = new ProductFolderSymbolHandler();
            handler.execute(rawResponse);
            products.addAll(handler.getProducts());
        });
        if (products.isEmpty()) logErrorToMap(response.getErrorMap(), NO_DATA, "for product in folder");
        return response;
    }

    @Override
    public MarketViewResponse<List<ProductFolder>> getProductFolders(@Nonnull MarketViewRequest request) {
        final MarketViewResponse<List<ProductFolder>> response = new MarketViewResponse<>();
        final List<ProductFolder> productFolders = new ArrayList<>();
        response.setResponse(productFolders);

        validateCredentials(request, response.getErrorMap());
        validateMandatoryParamsForGetProductFolders(request, response.getErrorMap());

        Optional<String> optionalRawResponse = prepareURLForGetProductFolders(request, response.getErrorMap())
                .filter(var -> response.getErrorMap().isEmpty())
                .map(url -> queryHelper.getRequest(request.getCredentials().getUsername() + "/" + request.getMarketViewSearchCriteria().getUserId(),
                        request.getCredentials().getPassword(),
                        url, response.getErrorMap()));

        optionalRawResponse.filter(var -> response.getErrorMap().isEmpty()).ifPresent(rawResponse -> {
            ProductFolderHandler handler = new ProductFolderHandler();
            handler.execute(rawResponse);
            productFolders.addAll(handler.getFolders());
        });
        if (productFolders.isEmpty()) logErrorToMap(response.getErrorMap(), NO_DATA, "for product folders");
        return response;
    }

    @Override
    public MarketViewResponse<List<ContractData>> getContractData(@Nonnull MarketViewRequest request) {
        final MarketViewResponse<List<ContractData>> response = new MarketViewResponse<>();
        final List<ContractData> contractDataList = new ArrayList<>();
        response.setResponse(contractDataList);

        validateCredentials(request, response.getErrorMap());
        validateMandatoryParamsForGetContractData(request, response.getErrorMap());

        if (response.getErrorMap().isEmpty()) {
            final String search = request.getMarketViewSearchCriteria().getSearch();
            final List<String> profiles = request.getMarketViewSearchCriteria().getProfiles();
            contractDataList.addAll(addContractsForRootOfRoots(request, search, profiles, response.getErrorMap()));
            contractDataList.addAll(fetchContractDataForProductProfiles(request, search, profiles, search, response.getErrorMap()));
        }
        if (contractDataList.isEmpty()) logErrorToMap(response.getErrorMap(), NO_DATA, "for contracts");
        return response;
    }

    @Override
    public MarketViewResponse<List<String>> getContractPeriods(@Nonnull MarketViewRequest request) {
        final MarketViewResponse<List<String>> response = new MarketViewResponse<>();
        final List<String> contractPeriods = new ArrayList<>();
        response.setResponse(contractPeriods);

        validateCredentials(request, response.getErrorMap());
        validateMandatoryParamsForGetContractPeriods(request, response.getErrorMap());

        final List<String> rawSymbols = new ArrayList<>();

        prepareURLForGetContractPeriods(request, response.getErrorMap()).filter(var -> response.getErrorMap().isEmpty())
                .ifPresent(url -> {
                    ChainHandler chainHandler = new ChainHandler();
                    try {
                        AvalancheConsumer.handleRequest(url,
                                request.getCredentials().getUsername(),
                                request.getCredentials().getPassword(),
                                chainHandler);
                    } catch (Exception e) {
                        logErrorToMap(response.getErrorMap(), ErrorCode.EXCEPTION, e.getMessage(),
                                "handling request for contract periods",
                                e.getCause().toString());
                    }
                    rawSymbols.addAll(chainHandler.getSymbols());
                });

        rawSymbols.forEach(symbol -> {
            String pc = transformer.convertPeriodCode(symbol);
            if (pc != null)
                contractPeriods.add(pc);
        });
        if (contractPeriods.isEmpty()) logErrorToMap(response.getErrorMap(), NO_DATA, "for contract periods");
        return response;
    }

    @Override
    public MarketViewResponse<List<ObjectObservation>> getTimeSeriesObservations(@Nonnull MarketViewRequest request) {
        final MarketViewResponse<List<ObjectObservation>> response = new MarketViewResponse<>();
        final List<ObjectObservation> observations = new ArrayList<>();
        response.setResponse(observations);

        validateCredentials(request, response.getErrorMap());
        validateMandatoryParamsForGetTimeSeriesObservations(request, response.getErrorMap());

        Optional<String> optionalRawResponse = prepareURLForGetTimeSeriesObservations(request, response.getErrorMap())
                .filter(var -> response.getErrorMap().isEmpty())
                .map(url -> queryHelper.getRequest(request.getCredentials().getUsername(),
                        request.getCredentials().getPassword(),
                        url, response.getErrorMap()));

        optionalRawResponse.filter(var -> response.getErrorMap().isEmpty())
                .ifPresent(rawResponse -> {
                    TimeSeriesHandler handler = new TimeSeriesHandler();
                    if (request.getMarketViewSearchCriteria().getBarInterval() != Integer.MIN_VALUE) {
                        handler.executeIntraDay(rawResponse);
                    } else {
                        handler.execute(rawResponse);
                    }
                    if (!handler.getObservations().isEmpty())
                        observations.addAll(handler.getObservations());
                });
        if (observations.isEmpty())
            logErrorToMap(response.getErrorMap(), NO_DATA, "for time series observations");
        return response;
    }

    private Optional<String> prepareURLForGetProducts(MarketViewRequest request, Map<String, String> errorMap) {
        Optional<String> finalUrl = Optional.empty();
        if (!errorMap.isEmpty()) return finalUrl;
        MarketViewSearchCriteria searchCriteria = request.getMarketViewSearchCriteria();
        try {
            URIBuilder uriBuilder = new URIBuilder(avalanche);
            uriBuilder.setCharset(StandardCharsets.UTF_8);
            uriBuilder.setPath("/UnifiedMetadata/csv");

            if (!Strings.isNullOrEmpty(searchCriteria.getUserId())) {
                uriBuilder.setParameter("user", searchCriteria.getUserId());
            } else {
                uriBuilder.setParameter("exact", "1");
            }

            if (!Strings.isNullOrEmpty(searchCriteria.getSearch())) {
                uriBuilder.setParameter("search", searchCriteria.getSearch());
            } else {
                uriBuilder.setParameter("roots", "1");
                if (!Strings.isNullOrEmpty(searchCriteria.getCommodity())) {
                    uriBuilder.setParameter("commodity", searchCriteria.getCommodity());
                }
                if (!Strings.isNullOrEmpty(searchCriteria.getSupplier())) {
                    uriBuilder.setParameter("supplier", searchCriteria.getSupplier());
                }
                if (!Strings.isNullOrEmpty(searchCriteria.getProduct())) {
                    uriBuilder.setParameter("product", searchCriteria.getProduct());
                }
                if (!Strings.isNullOrEmpty(searchCriteria.getMarketBasis())) {
                    uriBuilder.setParameter("marketbasis", searchCriteria.getMarketBasis());
                }
            }

            if (searchCriteria.getPage() != -1) {
                uriBuilder.setParameter("page", String.valueOf(searchCriteria.getPage()));
                uriBuilder.setParameter("pageSize", String.valueOf(searchCriteria.getPageSize()));
            }
            if (!Strings.isNullOrEmpty(searchCriteria.getCategory())) {
                uriBuilder.setParameter("exchange", searchCriteria.getCategory());
            }
            finalUrl = Optional.of(uriBuilder.toString());
        } catch (Exception e) {
            logErrorToMap(errorMap, ErrorCode.EXCEPTION, e.getMessage(),
                    "preparing url for products",
                    e.getCause().toString());
        }
        return finalUrl;
    }

    private Optional<Product> getProduct(MarketViewCredentials credentials, String search, Map<String, String> errorMap) {
        final MarketViewRequest productRequest = new MarketViewRequest()
                .setCredentials(credentials)
                .setMarketViewSearchCriteria(new MarketViewSearchCriteria().setSearch(search));
        Optional<Product> product = Optional.empty();
        final MarketViewResponse<List<Product>> marketViewResponse = getProducts(productRequest);
        List<Product> products = marketViewResponse.getResponse();
        errorMap.putAll(marketViewResponse.getErrorMap());
        if (!products.isEmpty()) {
            product = Optional.of(products.get(0));
        }
        return product;
    }

    private Optional<String> prepareURLForGetProductFolders(MarketViewRequest request, Map<String, String> errorMap) {
        Optional<String> finalURL = Optional.empty();
        if (!errorMap.isEmpty()) return finalURL;
        MarketViewSearchCriteria searchCriteria = request.getMarketViewSearchCriteria();
        try {
            URIBuilder uriBuilder = new URIBuilder(avalanche);
            uriBuilder.setCharset(StandardCharsets.UTF_8);
            uriBuilder.setPath("/GetSymbolTreeBranch/csv");
            uriBuilder.setParameter("folderid", searchCriteria.getFolderId());
            finalURL = Optional.of(uriBuilder.toString());
        } catch (Exception e) {
            logErrorToMap(errorMap, ErrorCode.EXCEPTION, e.getMessage(),
                    "preparing url for get product folders",
                    e.getCause().toString());
        }
        return finalURL;
    }

    private List<ContractData> addContractsForRootOfRoots(MarketViewRequest request, String search, List<String> profiles, Map<String, String> errorMap) {
        final List<ContractData> contractDataList = new ArrayList<>();
        getProduct(request.getCredentials(), search, errorMap).filter(var -> errorMap.isEmpty())
                .ifPresent(product -> {
                    if (product.hasProperty("rootofroots") &&
                            "true".equalsIgnoreCase(CurveProperty.getPropertyByName(product.getProperties(), "rootofroots").getValue())) {

                        Optional<String> optionalSymbolRoot = Optional.of(CurveProperty.getPropertyByName(product.getProperties(), "option_root") != null ?
                                Strings.nullToEmpty(CurveProperty.getPropertyByName(product.getProperties(), "option_root").getValue()) : "");

                        optionalSymbolRoot
                                .filter(Predicate.not(String::isEmpty))
                                .ifPresent(symbolRoot -> {
                                    String[] roots = symbolRoot.split(",");
                                    List<ContractData> allProfilesDataList = new ArrayList<>();
                                    for (String symbol : roots) {
                                        allProfilesDataList.addAll(fetchContractDataForProductProfiles(request, symbol, profiles, search, errorMap));
                                    }
                                    Map<String, List<ContractData>> groupedProfiles = allProfilesDataList.stream()
                                            .collect(Collectors.groupingBy(ContractData::getProfile));

                                    groupedProfiles.forEach((key, dataList) -> {
                                        ContractData consolidatedContract = null;
                                        for (ContractData data : dataList) {
                                            if (Objects.isNull(consolidatedContract))
                                                consolidatedContract = data;
                                            else
                                                consolidatedContract.add(data);
                                        }
                                        if (Objects.nonNull(consolidatedContract)) {
                                            ContractData finalContract = new ContractData(consolidatedContract.getSourceIdentity(),
                                                    search,
                                                    consolidatedContract.getProfile().toUpperCase(),
                                                    consolidatedContract.getOndate(),
                                                    consolidatedContract.getIndex());
                                            finalContract.add(consolidatedContract);
                                            contractDataList.add(finalContract);
                                        }
                                    });
                                });
                    }
                });
        return contractDataList;
    }

    private List<ContractData> fetchContractDataForProductProfiles(MarketViewRequest request, String symbol, List<String> profiles, String root, Map<String, String> errorMap) {

        MarketViewCredentials credentials = request.getCredentials();
        List<ContractData> contractDataList = new ArrayList<>();

        return getProduct(credentials, symbol, errorMap)
                .filter(var -> errorMap.isEmpty())
                .map(product -> {
                    Optional<String> optionalRawResponse;
                    final boolean forwardCurve = isForwardCurve(product);

                    optionalRawResponse = ((forwardCurve) ? prepareURLForGetForwardCurves(request, symbol, profiles, errorMap) :
                            prepareURLForGetDailyData(request, symbol, profiles, errorMap))
                            .filter(var -> errorMap.isEmpty())
                            .map(url -> queryHelper.getRequest(credentials.getUsername(),
                                    credentials.getPassword(), url, errorMap));

                    optionalRawResponse.filter(var -> errorMap.isEmpty()).ifPresent(response -> {
                        LocalDate onDate = (Objects.isNull(request.getMarketViewSearchCriteria().getOnDate())) ? LocalDate.now() : request.getMarketViewSearchCriteria().getOnDate();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                        String date = formatter.format(onDate);
                        final long index = onDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                        profiles.forEach(profile -> {
                            ContractDataHandler handler = new ContractDataHandler(transformer).setRoot(root);
                            handler.setProfile(profile);
                            if ((forwardCurve)) {
                                handler.executeForwardContractData(response, profile);
                            } else {
                                handler.executeDailyData(response, profile);
                            }
                            Optional<ContractData> optionalContractData = Optional.ofNullable(handler.getContractData());
                            optionalContractData.ifPresent(contractData -> {
                                ContractData data = new ContractData("MARKETVIEW", symbol, profile,
                                        date, index);
                                data.add(contractData);
                                contractDataList.add(data);
                            });
                        });
                    });
                    return contractDataList;
                }).orElse(Collections.emptyList());
    }

    private Optional<String> prepareURLForGetDailyData(MarketViewRequest request, String symbol, List<String> profiles, Map<String, String> errorMap) {
        Optional<String> finalURL = Optional.empty();
        if (!errorMap.isEmpty()) return finalURL;
        MarketViewSearchCriteria searchCriteria = request.getMarketViewSearchCriteria();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        String fields = profiles.stream().map(String::toLowerCase).collect(Collectors.joining(","));
        try {
            URIBuilder uriBuilder = new URIBuilder(webservice);
            uriBuilder.setCharset(StandardCharsets.UTF_8);
            uriBuilder.setPath(uriBuilder.getPath() + "/getdaily");
            uriBuilder.setParameter("symbols", symbol);
            uriBuilder.setParameter("output", "csv");
            uriBuilder.setParameter("fields", "symbol," + fields);
            if (searchCriteria.getDaysBack() != Integer.MIN_VALUE) {
                uriBuilder.setParameter("daysback", String.valueOf(searchCriteria.getDaysBack()));
            } else if (!Objects.isNull(searchCriteria.getStartDate()) && !Objects.isNull(searchCriteria.getEndDate())) {
                uriBuilder.setParameter("startdate", formatter.format(searchCriteria.getStartDate()));
                uriBuilder.setParameter("enddate", formatter.format(searchCriteria.getEndDate()));
            }
            if (searchCriteria.isSettledOnly()) {
                uriBuilder.setParameter("settledonly", "true");
            }
            finalURL = Optional.of(uriBuilder.toString());
        } catch (Exception e) {
            logErrorToMap(errorMap, ErrorCode.EXCEPTION, e.getMessage(),
                    "preparing url for daily data",
                    e.getCause().toString());
        }
        return finalURL;
    }

    private Optional<String> prepareURLForGetForwardCurves(MarketViewRequest request, String symbol, List<String> profiles,
                                                           Map<String, String> errorMap) {
        Optional<String> finalURL = Optional.empty();
        if (!errorMap.isEmpty()) return finalURL;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String fields = profiles.stream().map(String::toLowerCase).collect(Collectors.joining(","));
        try {
            URIBuilder uriBuilder = new URIBuilder(webservice);
            uriBuilder.setCharset(StandardCharsets.UTF_8);
            uriBuilder.setPath(uriBuilder.getPath() + "/getdailycurve");
            uriBuilder.setParameter("curveroots", symbol);
            uriBuilder.setParameter("output", "csv");
            uriBuilder.setParameter("fields", "symbol," + fields);
            if (!Objects.isNull(request.getMarketViewSearchCriteria().getOnDate()))
                uriBuilder.setParameter("curvedate", formatter.format(request.getMarketViewSearchCriteria().getOnDate()));
            finalURL = Optional.of(uriBuilder.toString());
        } catch (Exception e) {
            logErrorToMap(errorMap, ErrorCode.EXCEPTION, e.getMessage(),
                    "preparing url for forward curves",
                    e.getCause().toString());
        }
        return finalURL;
    }

    private Optional<String> prepareURLForGetContractPeriods(MarketViewRequest request, Map<String, String> errorMap) {
        Optional<String> finalURL = Optional.empty();
        if (!errorMap.isEmpty()) return finalURL;
        try {
            URIBuilder uriBuilder = new URIBuilder(webservice);
            uriBuilder.setCharset(StandardCharsets.UTF_8);
            uriBuilder.setPath(uriBuilder.getPath() + "/getchain");
            uriBuilder.setParameter("optionroot", request.getMarketViewSearchCriteria().getSearch());
            uriBuilder.setParameter("fields", "symbol,close");
            finalURL = Optional.of(uriBuilder.toString());
        } catch (Exception e) {
            logErrorToMap(errorMap, EXCEPTION, e.getMessage(),
                    "preparing url for contract periods",
                    e.getCause().toString());
        }
        return finalURL;
    }

    private Optional<String> prepareURLForGetTimeSeriesObservations(MarketViewRequest request, Map<String, String> errorMap) {
        Optional<String> response = Optional.empty();
        if (!errorMap.isEmpty()) return response;
        try {
            URIBuilder uriBuilder = new URIBuilder(webservice);
            uriBuilder.setCharset(StandardCharsets.UTF_8);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
            if (request.getMarketViewSearchCriteria().getBarInterval() == Integer.MIN_VALUE) {
                uriBuilder.setPath(uriBuilder.getPath() + "/getdaily");
            } else {
                uriBuilder.setPath(uriBuilder.getPath() + "/getintraday");
                uriBuilder.setParameter("intradaybarinterval", String.valueOf(request.getMarketViewSearchCriteria().getBarInterval()));
            }
            uriBuilder.setParameter("symbols", request.getMarketViewSearchCriteria().getSearch());
            uriBuilder.setParameter("fields", "symbol,date," + request.getMarketViewSearchCriteria().getProfile().toLowerCase());
            uriBuilder.setParameter("startdate", formatter.format(request.getMarketViewSearchCriteria().getStartDate()));
            uriBuilder.setParameter("enddate", formatter.format(request.getMarketViewSearchCriteria().getEndDate()));
            uriBuilder.setParameter("output", "csv");
            response = Optional.of(uriBuilder.toString());
        } catch (Exception e) {
            logErrorToMap(errorMap, EXCEPTION, e.getMessage(),
                    "preparing url for time series observation",
                    e.getCause().toString());
        }
        return response;
    }
}
