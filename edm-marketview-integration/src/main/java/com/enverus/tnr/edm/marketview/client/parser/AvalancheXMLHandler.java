package com.enverus.tnr.edm.marketview.client.parser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

public class AvalancheXMLHandler extends DefaultHandler {

    protected Map<String, String> properties = new HashMap<>();

    public static final Map<String, String> gvFrequencyMap = new TreeMap<>();
    public static final Map<String, String> monthOffset = new TreeMap<>();


    protected String getString(Attributes attributes, String name) {
        if (attributes.getIndex(name) == -1)
            return null;
        return attributes.getValue(name).trim();
    }

    protected double getDouble(Attributes attributes, String name) {
        String value = getString(attributes, name);
        double v = Double.NaN;
        if (value != null && !value.equals(""))
            v = Double.parseDouble(value);
        return v;
    }

    protected String[] splitLine(String line) {
        List<String> parts = new ArrayList<>();
        int ch = 0;
        while (ch < line.length()) {
            if (line.charAt(ch) == '"') {
                int cq = line.indexOf("\"", ch + 1);
                parts.add(line.substring(ch + 1, cq));
                ch = cq + 1;
            } else {
                int cq = line.indexOf(",", ch);
                if (cq == -1) {
                    parts.add("");
                    break;
                } else {
                    parts.add(line.substring(ch, cq));
                    ch = cq;
                }
            }
            ch++;
        }
        return parts.toArray(new String[0]);
    }

    public void execute(String csvResults) throws SAXException {
        properties = new HashMap<>();
        String[] parts = csvResults.split("\n");
        int max = parts.length;
        if (parts.length > 0) {
            String[] headerParts = parts[0].split(",");
            for (int i = 1; i < max; i++) {
                String[] lineParts = parts[i].split(",");
                properties.put(headerParts[i], lineParts[i]);
            }
        }

    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

}
