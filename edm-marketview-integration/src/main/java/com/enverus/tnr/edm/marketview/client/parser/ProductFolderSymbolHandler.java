package com.enverus.tnr.edm.marketview.client.parser;

import com.datagenicgroup.curvebuilder.entity.Product;
import com.datagenicgroup.curvebuilder.entity.ProductBuilder;

import java.util.ArrayList;
import java.util.List;

public class ProductFolderSymbolHandler extends AvalancheXMLHandler {
    private final List<Product> products = new ArrayList<>();

    public List<Product> getProducts() {
        return products;
    }

    @Override
    public void execute(String csvResults) {
        String[] parts = csvResults.split("\r\n");
        for (int i=1; i<parts.length; i++) {
            String[] data = splitLine(parts[i]);
            if (data[12].equals("1")) {
                // Only get symbols
                ProductBuilder product = new ProductBuilder()
                        .setCode(data[2])
                        .setDescription(data[1])
                        .setName(data[1])
                        .setCategory(data[3])
                        .setSource("MARKETVIEW")
                        .setDefaultProfile("CLOSE")
                        .setId(data[2])
                        .setCurrency(data[5])
                        .setUnits(data[6]);
                products.add(product.build());
            }
        }
    }

}
