package com.enverus.tnr.edm.marketview.client.parser;

import com.datagenicgroup.curvebuilder.entity.ProductFolder;

import java.util.ArrayList;
import java.util.List;

public class ProductFolderHandler extends AvalancheXMLHandler {
    private final List<ProductFolder> folders = new ArrayList<>();

    public List<ProductFolder> getFolders() {
        return folders;
    }

    @Override
    public void execute(String csvResults) {
        String[] parts = csvResults.split("\r\n");
        for (int i=1; i<parts.length; i++) {
            String[] data = splitLine(parts[i]);
            if (data[12].equals("0")) {
                // Only get folders
                ProductFolder folder = new ProductFolder()
                        .setId(data[0])
                        .setDescription(data[1]);
                folders.add(folder);
            }
        }
    }
}
