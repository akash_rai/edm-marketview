package com.datagenic.test;

import com.enverus.tnr.edm.marketview.client.configuration.Configuration;
import com.enverus.tnr.edm.marketview.client.parser.PeriodCodeTransformer;
import com.enverus.tnr.edm.marketview.client.rest.RESTQueryHelper;
import com.datagenicgroup.curvebuilder.entity.ContractData;
import com.datagenicgroup.timeseries.ObjectObservation;
import com.enverus.tnr.edm.marketview.client.impl.MarketViewClientAPI;
import com.enverus.tnr.edm.marketview.entity.MarketViewCredentials;
import com.enverus.tnr.edm.marketview.entity.MarketViewRequest;
import com.enverus.tnr.edm.marketview.entity.MarketViewResponse;
import com.enverus.tnr.edm.marketview.entity.MarketViewSearchCriteria;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class MarketViewClientAPITests {
    public static MarketViewClientAPI clientAPI;
    public static MarketViewCredentials credentials;
    public static PeriodCodeTransformer periodCodeTransformer;

    @BeforeClass
    public static void init() {
        final String username = "dg_delayed";
        final String password = "S34!fgholet";
        final String avalancheURL = "https://webservice.gvsi.com/gvsi/xml";
        final String avalancheQuery = "https://query.gvsi.com";
        final String database = "mongodb://localhost:27017";

        RESTQueryHelper restQueryHelper = new RESTQueryHelper();
        MongoClient client = MongoClients.create(database);
        Configuration configuration = new Configuration()
                .setUsername(username)
                .setPassword(password)
                .setMongoDB(database);

        periodCodeTransformer = new PeriodCodeTransformer(client, configuration, restQueryHelper);

        clientAPI = new MarketViewClientAPI(configuration.getAvalanche(),configuration.getWebservice(),
                restQueryHelper, periodCodeTransformer);

        credentials = new MarketViewCredentials().setUsername(username).setPassword(password);
    }

    @Test
    public void should_fetch_product_for_exact_symbol() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria().setSearch("/C"));
        clientAPI.getProducts(marketViewRequest);
    }

    @Test
    public void should_fetch_product_for_userId_And_symbol() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria().setSearch("/C").setUserId("root"));
        clientAPI.getProducts(marketViewRequest);
    }

    @Test
    public void should_fetch_product_for_userId() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria().setUserId("root"));
        clientAPI.getProducts(marketViewRequest);
    }

    @Test
    public void should_fetch_product_folders_for_userId() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria().setUserId("1dghusky"));
        clientAPI.getProductsForFolder(marketViewRequest);
    }

    @Test
    public void should_fetch_get_periods_for_userId() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria().setSearch("/GCL"));
        clientAPI.getContractPeriods(marketViewRequest);
    }

    @Test
    public void should_get_contract_data() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria().setSearch("#ACOFC4501420221").setProfiles(Arrays.asList("open", "high", "low", "close", "volume", "midpoint")).setOnDate(LocalDate.now().minusDays(4)));

        clientAPI.getContractData(marketViewRequest);
    }

    @Test
    public void should_get_contract_data2() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria()
                .setSearch("#NRG.NRGCL.CAPC.TKB25")
                .setProfiles(Collections.singletonList("close")).setOnDate(LocalDate.of(2018, 8, 31)));
        long start = System.currentTimeMillis();
        MarketViewResponse<List<ContractData>> contractData = clientAPI.getContractData(marketViewRequest);
        for (ContractData data : contractData.getResponse()) {
            data.getValues().forEach(v -> System.out.println(v.getPeriodCode() + "=" + v.getValue()));
        }
        System.out.println("Elapsed = " + (System.currentTimeMillis() - start));
    }

    @Test
    public void should_get_timeseries_obs() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria().setSearch("#AESO00005500000001").setProfile("close")
                .setStartDate(LocalDateTime.of(2021, 2, 17, 0, 0).minusDays(2))
                .setBarInterval(1800 / 60));
        long start = System.currentTimeMillis();
        MarketViewResponse<List<ObjectObservation>> contractData = clientAPI.getTimeSeriesObservations(marketViewRequest);
        for (ObjectObservation data : contractData.getResponse()) {
            System.out.println(data.getValue());
        }
        System.out.println("Elapsed = " + (System.currentTimeMillis() - start));
    }

    @Test
    public void should_get_timeseries_obs2() {
        MarketViewRequest marketViewRequest = new MarketViewRequest();
        marketViewRequest.setCredentials(credentials);
        marketViewRequest.setMarketViewSearchCriteria(new MarketViewSearchCriteria().setSearch("#AESO00005500000001").setProfile("close")
                .setEndDate(LocalDateTime.of(2021, 2, 17, 0, 0))
                .setBarInterval(1800 / 60));
        long start = System.currentTimeMillis();
        MarketViewResponse<List<ObjectObservation>> contractData = clientAPI.getTimeSeriesObservations(marketViewRequest);
        for (ObjectObservation data : contractData.getResponse()) {
            System.out.println(data.getValue());
        }
        System.out.println("Elapsed = " + (System.currentTimeMillis() - start));
    }

    @Test
    public void test() {
        Pattern pattern = Pattern.compile("[\"\r]");
        String line = "#AESO00005500000001,8/2/2020 12:00:00 AM,160.35\r";
        String[] lineParts = line.split(",");
        String dateTime = lineParts[1].replace("\"", "");

        long t2 = System.nanoTime();
        String value2 = pattern.matcher(lineParts[2]).replaceAll("");
        System.out.println(Double.parseDouble(value2));
        System.out.println("T2:::" + (System.nanoTime() - t2));

        long t1 = System.nanoTime();
        String value = lineParts[2].replace("\"", "");
        System.out.println(Double.parseDouble(value));
        System.out.println("T1:::" + (System.nanoTime() - t1));

        System.out.println(dateTime);
    }

    @Test
    public void testMongo() throws Exception {
        periodCodeTransformer.setConfiguration(new Configuration().setUsername("dg_delayed").setPassword("S34!fgholet"));
        System.out.println(periodCodeTransformer.convertPeriodCode("#D.NAGME19", "#D.NAGM"));
//        Assert.assertEquals("2019M05", periodCodeTransformer.convertPeriodCode("#D.NAGME19", "#D.NAGM"));
    }

    @Test
    public void testAmerex() throws Exception {
        periodCodeTransformer.setConfiguration(new Configuration().setUsername("dg_delayed").setPassword("S34!fgholet").setPeriodCodeCacheExpiry(5));
        Assert.assertEquals("2019M05", periodCodeTransformer.convertPeriodCode("#AMXALLP.AEPD.MK19", "#AMXALLP.AEPD"));
        Assert.assertEquals("2019Y", periodCodeTransformer.convertPeriodCode("#AMXALLP.AEPD.YF19", "#AMXALLP.AEPD.YF19"));
    }

    @Test
    public void testGas() throws Exception {
        Assert.assertEquals("2018M10", periodCodeTransformer.convertPeriodCode("/GASV18", "/GAS"));
    }

    @Test
    public void testMonths() throws Exception {
        Assert.assertEquals("2017M11", periodCodeTransformer.convertPeriodCode("/GWMX17"));
        Assert.assertEquals("2017M12", periodCodeTransformer.convertPeriodCode("/GWMZ17"));
    }

    @Test
    public void testYears() throws Exception {
        Assert.assertEquals("2018Y", periodCodeTransformer.convertPeriodCode("#HRYF18", "#HR"));
        Assert.assertEquals("2018Y", periodCodeTransformer.convertPeriodCode("#HRCYF18", "#HR"));
    }

    @Test
    public void testQuarters() throws Exception {
        Assert.assertEquals("2018Q01", periodCodeTransformer.convertPeriodCode("/GWMF18-H18"));
        Assert.assertEquals("2018Q01", periodCodeTransformer.convertPeriodCode("#HR0QF18", "#HRO"));
        Assert.assertEquals("2018Q02", periodCodeTransformer.convertPeriodCode("#HR0QJ18", "#HRO"));
        Assert.assertEquals("2018Q03", periodCodeTransformer.convertPeriodCode("#HR0QN18", "#HRO"));
        Assert.assertEquals("2018Q04", periodCodeTransformer.convertPeriodCode("#HR0QV18", "#HRO"));
        Assert.assertEquals("2018SS", periodCodeTransformer.convertPeriodCode("#HR0SJ18", "#HRO"));
        Assert.assertEquals("2018SW", periodCodeTransformer.convertPeriodCode("#HR0SV18", "#HRO"));
        Assert.assertEquals("2018Y", periodCodeTransformer.convertPeriodCode("#HR0YF18", "#HRO"));
    }

    @Test
    public void testDays() throws Exception {
        Assert.assertEquals("20180301", periodCodeTransformer.convertPeriodCode("#WFSCSSD01H18", "#WFSCSSD"));
        Assert.assertEquals("2018M03", periodCodeTransformer.convertPeriodCode("#WFSCSSDMMH18", "#WFSCSSD"));
    }

    @Test
    public void testFinancial() throws Exception {
        Assert.assertEquals("ON", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCCON"));
        Assert.assertEquals("TN", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCCTN"));
        Assert.assertEquals("SN", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCCSN"));
        Assert.assertEquals("SW", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCCSW"));
        Assert.assertEquals("1W", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC1W"));
        Assert.assertEquals("1M", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC1M"));
        Assert.assertEquals("2M", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC2M"));
        Assert.assertEquals("3M", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC3M"));
        Assert.assertEquals("6M", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC6M"));
        Assert.assertEquals("9M", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC9M"));
        Assert.assertEquals("1Y", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC1Y"));
        Assert.assertEquals("2Y", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC2Y"));
        Assert.assertEquals("3Y", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC3Y"));
        Assert.assertEquals("4Y", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC4Y"));
        Assert.assertEquals("5Y", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC5Y"));
        Assert.assertEquals("7Y", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC7Y"));
        Assert.assertEquals("10Y", periodCodeTransformer.convertPeriodCode("#WFS.IRZR.USDZRCC10Y"));
    }

    @Test
    public void testPlatts() throws Exception {
        periodCodeTransformer.setConfiguration(new Configuration().setUsername("dg_delayed").setPassword("S34!fgholet"));
        Assert.assertEquals("SPOT", periodCodeTransformer.convertPeriodCode("#AALRI00"));
        Assert.assertEquals("M01", periodCodeTransformer.convertPeriodCode("#PA00056112MM01", "#PA00056112MM01"));
    }

    @Test
    public void testCBOB() throws Exception {
        periodCodeTransformer.setConfiguration(new Configuration().setUsername("dg_delayed").setPassword("S34!fgholet"));
        String periodCode = periodCodeTransformer.convertPeriodCode("#X.FUSDCAD\\01M", "#X.FUSDCAD\\01M");
        System.out.println(periodCode);
        Assert.assertEquals("1M", periodCode);
    }


    @Test
    public void testCache() throws Exception {
        periodCodeTransformer.setConfiguration(new Configuration().setUsername("dg_sa").setPassword("S34!fgholet").setPeriodCodeCacheExpiry(5));
        long start = System.currentTimeMillis();
        Assert.assertEquals("SPOT", periodCodeTransformer.convertPeriodCode("#AALRI00"));
        long e1 = System.currentTimeMillis() - start;
        System.out.println(e1);

        start = System.currentTimeMillis();
        Assert.assertEquals("SPOT", periodCodeTransformer.convertPeriodCode("#AALRI00"));
        long e2 = System.currentTimeMillis() - start;
        System.out.println(e2);
        Assert.assertTrue(e2 < e1);

        Thread.sleep(6000);
        start = System.currentTimeMillis();
        Assert.assertEquals("SPOT", periodCodeTransformer.convertPeriodCode("#AALRI00"));
        long e3 = System.currentTimeMillis() - start;
        System.out.println(e3);
        Assert.assertTrue(e3 > e2);

    }

    @Test
    public void testHalfMonths() throws Exception {
        Assert.assertEquals("2018HM11", periodCodeTransformer.convertPeriodCode("#GNGACFRJNAPH1M18", "#GNGACFRJNAPH"));
    }

    @Test
    public void testEEXWeekly() throws Exception {
        Assert.assertEquals("2019W47", periodCodeTransformer.convertPeriodCode("/E.W1B03X19", "/E.W1B_WEEK"));
    }

    @Test
    public void testPVMWeekly() throws Exception {
        Assert.assertEquals("2019W48", periodCodeTransformer.convertPeriodCode("#PVM044629USCAVGW4819", "#PVM044629USCAVGW"));
    }

    @Test
    public void testFUSMWeekly() throws Exception {
        Assert.assertEquals("1W", periodCodeTransformer.convertPeriodCode("#X.FUSDCAD\\01W", "#X.FUSDCAD\\01W"));
    }

    @Test
    public void testGCB2713() throws Exception {
        Assert.assertEquals("2020M03", periodCodeTransformer.convertPeriodCode("#PVM011288DMAVGXH20H20", "##PVM011288DMAVG_SPREAD1"));
    }
}
